<?php


namespace Dracoder\RepositoryBundle;

use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Dracoder\FilterBundle\Filter;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Class AppRepository
 *
 * @package App\Repository
 */
abstract class AppRepository extends ServiceEntityRepository
{
    /**
     * AppRepository constructor.
     *
     * @param ManagerRegistry $manager
     */
    public function __construct(ManagerRegistry $manager)
    {
        parent::__construct($manager, $this->getEntityClassName());
    }

    /**
     * @return string
     */
    abstract public function getEntityClassName(): string;

    /**
     * Get SQL from query
     *
     * @param Query $query
     *
     * @return string
     * @author Yosef Kaminskyi
     *
     * @see https://stackoverflow.com/questions/2095394/doctrine-how-to-print-out-the-real-sql-not-just-the-prepared-statement/23545733
     *
     */
    public function getFullSQL(Query $query): string
    {
        $sql = $query->getSql();
        $paramsList = $this->getListParamsByDql($query->getDql());
        $paramsArr = $this->getParamsArray($query->getParameters());
        $fullSql = '';
        for ($i = 0, $iMax = strlen($sql); $i < $iMax; $i++) {
            if ($sql[$i] === '?') {
                $nameParam = array_shift($paramsList);

                if (is_string($paramsArr[$nameParam])) {
                    $fullSql .= '"' . addslashes($paramsArr[$nameParam]) . '"';
                } elseif (is_array($paramsArr[$nameParam])) {
                    $sqlArr = '';
                    foreach ($paramsArr[$nameParam] as $var) {
                        if (!empty($sqlArr)) {
                            $sqlArr .= ',';
                        }

                        if (is_string($var)) {
                            $sqlArr .= '"' . addslashes($var) . '"';
                        } else {
                            $sqlArr .= $var;
                        }
                    }
                    $fullSql .= $sqlArr;
                } elseif (is_object($paramsArr[$nameParam])) {
                    if ($paramsArr[$nameParam] instanceof DateTime) {
                        $fullSql .= "'" . $paramsArr[$nameParam]->format('Y-m-d H:i:s') . "'";
                    } else {
                        $fullSql .= $paramsArr[$nameParam]->getId();
                    }

                } else {
                    $fullSql .= $paramsArr[$nameParam];
                }
            } else {
                $fullSql .= $sql[$i];
            }
        }

        return $fullSql;
    }

    /**
     * @param string $dql
     *
     * @return array
     */
    public function getListParamsByDql(string $dql): array
    {
        $parsedDql = explode(":", $dql);
        $length = count($parsedDql);
        $parameters = [];
        for ($i = 1; $i < $length; $i++) {
            if (ctype_alpha($parsedDql[$i][0])) {
                $param = (preg_split("/[' )]/", $parsedDql[$i]));
                $parameters[] = $param[0];
            }
        }

        return $parameters;
    }

    /**
     * @param ArrayCollection $paramObj
     *
     * @return array
     */
    private function getParamsArray(ArrayCollection $paramObj): array
    {
        $parameters = [];
        foreach ($paramObj as $val) {
            $parameters[$val->getName()] = $val->getValue();
        }

        return $parameters;
    }

    /**
     * @param Filter[] $filters
     *
     * @return QueryBuilder
     */
    public function getQueryBuilder(array $filters = []): QueryBuilder
    {
        $qb = $this->createQueryBuilder($this->getAlias());

        foreach ($filters as $filter) {
            $filter->apply($qb);
        }

        return $qb;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        $classNameAsArray = explode('\\', $this->getEntityClassName());
        $shortName = $classNameAsArray[count($classNameAsArray)-1];
        $snakeCaseConverter = new CamelCaseToSnakeCaseNameConverter();

        return $snakeCaseConverter->normalize($shortName);
    }

    /**
     * @param QueryBuilder $qb
     * @param array $filters
     *
     * @return $this
     */
    protected function applyFilters(QueryBuilder $qb, array $filters): self
    {
        foreach ($filters as $filter) {
            $filter->apply($qb);
        }

        return $this;
    }
}
