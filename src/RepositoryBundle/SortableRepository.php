<?php

namespace Dracoder\RepositoryBundle;

use Doctrine\ORM\QueryBuilder;
use Dracoder\FilterBundle\Filter;

abstract class SortableRepository extends AppRepository
{
    /**
     * @param Filter[] $filters
     * @param array $sorting
     *
     * @return QueryBuilder
     */
    public function getQueryBuilder(array $filters = [], array $sorting = []): QueryBuilder
    {
        $qb = parent::getQueryBuilder($filters);

        $this->setQueryBuilderSorting($qb, $sorting);

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param array $sorting
     */
    protected function setQueryBuilderSorting(QueryBuilder $qb, array $sorting): void
    {
        if ($sorting) {
            foreach (array_keys($sorting) as $sortField) {
                if (in_array($sortField, $this->getClassMetadata()->fieldNames, true)) {
                    $direction = (mb_strtolower($sorting[$sortField]) === 'asc') ? 'asc' : 'desc';
                    $field = $this->getClassMetadata()->getFieldName($sortField);
                    $qb->addOrderBy($this->getAlias() . '.' . $field, $direction);
                }
            }
        }
    }
}
